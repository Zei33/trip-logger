package net.zeiworld.triplogger;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import net.zeiworld.triplogger.database.TripBaseHelper;
import net.zeiworld.triplogger.database.TripCursorWrapper;
import net.zeiworld.triplogger.database.TripDbSchema;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TripStore {

    private static TripStore sTripStore;

    private Context mContext;
    private SQLiteDatabase mDatabase;

    public static TripStore get(Context context) {
        if (sTripStore == null) {
            sTripStore = new TripStore(context);
        }
        return sTripStore;
    }

    private TripStore(Context context) {

        mContext = context.getApplicationContext();
        mDatabase = new TripBaseHelper(mContext)
                .getWritableDatabase();

    }

    public void addTrip(Trip c) {
        ContentValues values = getContentValues(c);
        mDatabase.insert(TripDbSchema.TripTable.NAME, null, values);
    }

    public void updateSettings(Settings settings) {
        String id = settings.getId();
        ContentValues values = getContentValuesSettings(settings);
        mDatabase.update(TripDbSchema.TripTable.SETTINGSNAME, values, TripDbSchema.TripTable.Cols.ID + " = ?", new String[] { id });
    }

    public void updateTrip(Trip trip) {
        String uuidString = trip.getId().toString();
        ContentValues values = getContentValues(trip);
        mDatabase.update(TripDbSchema.TripTable.NAME, values, TripDbSchema.TripTable.Cols.UUID + " = ?", new String[] { uuidString });
    }

    public void deleteTrip(Trip trip) {
        String uuidString = trip.getId().toString();
        mDatabase.delete(TripDbSchema.TripTable.NAME, TripDbSchema.TripTable.Cols.UUID + " = ?", new String[]{ uuidString });
    }

    public List<Trip> getTrips() {
        List<Trip> trips = new ArrayList<>();
        TripCursorWrapper cursor = queryTrips(null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                trips.add(cursor.getTrip());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return trips;
    }
    public Trip getTrip(UUID id) {
        TripCursorWrapper cursor = queryTrips(TripDbSchema.TripTable.Cols.UUID + " = ?", new String[] { id.toString() });
        try {
            if (cursor.getCount() == 0) {
                return null;
            }
            cursor.moveToFirst();
            return cursor.getTrip();
        } finally {
            cursor.close();
        }
    }

    public Settings getSettings(String id) {
        TripCursorWrapper cursor = querySettings(TripDbSchema.TripTable.Cols.ID + " = ?", new String[] { id });
        try {
            if (cursor.getCount() == 0) {
                return new Settings("1067958");
            }
            cursor.moveToFirst();
            return cursor.getSettings();
        } finally {
            cursor.close();
        }
    }

    public File getPhotoFile(Trip trip) {
        File externalFilesDir = mContext
                .getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (externalFilesDir == null) {
            return null;
        }
        return new File(externalFilesDir, trip.getPhotoFilename());
    }

    private static ContentValues getContentValues(Trip trip) {
        ContentValues values = new ContentValues();
        values.put(TripDbSchema.TripTable.Cols.UUID, trip.getId().toString());
        values.put(TripDbSchema.TripTable.Cols.TITLE, trip.getTitle());
        values.put(TripDbSchema.TripTable.Cols.DATE, trip.getDate().getTime());
        values.put(TripDbSchema.TripTable.Cols.TYPE, trip.getType());
        values.put(TripDbSchema.TripTable.Cols.DESTINATION, trip.getDestination());
        values.put(TripDbSchema.TripTable.Cols.DURATION, trip.getDuration());
        values.put(TripDbSchema.TripTable.Cols.COMMENT, trip.getComment());
        values.put(TripDbSchema.TripTable.Cols.PHOTO, trip.getPhoto());
        values.put(TripDbSchema.TripTable.Cols.COMPLETED, trip.isComplete() ? 1 : 0);
        return values;
    }

    private static ContentValues getContentValuesSettings(Settings settings) {
        ContentValues values = new ContentValues();
        values.put(TripDbSchema.TripTable.Cols.ID, settings.getId());
        values.put(TripDbSchema.TripTable.Cols.DISPLAYNAME, settings.getName());
        values.put(TripDbSchema.TripTable.Cols.EMAIL, settings.getEmail());
        values.put(TripDbSchema.TripTable.Cols.COMMENT, settings.getComment());
        values.put(TripDbSchema.TripTable.Cols.GENDER, settings.getGender());
        return values;
    }

    private TripCursorWrapper queryTrips(String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
            TripDbSchema.TripTable.NAME,
            null,
            whereClause,
            whereArgs,
            null,
            null,
            null
        );
        return new TripCursorWrapper(cursor);
    }

    private TripCursorWrapper querySettings(String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                TripDbSchema.TripTable.SETTINGSNAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );
        return new TripCursorWrapper(cursor);
    }

}
