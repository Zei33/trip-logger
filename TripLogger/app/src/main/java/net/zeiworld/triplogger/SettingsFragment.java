package net.zeiworld.triplogger;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioGroup;

import java.io.File;
import java.util.UUID;

public class SettingsFragment extends Fragment {

    private EditText mNameField;
    private EditText mEmailField;
    private EditText mIDField;
    private EditText mCommentField;
    private RadioGroup mGenderGroup;
    private Settings mSettings;

    public static SettingsFragment newInstance() {
        Bundle args = new Bundle();
        SettingsFragment fragment = new SettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String settingsID = "1";
        mSettings = TripStore.get(getActivity()).getSettings(settingsID);
    }

    @Override
    public void onPause() {
        super.onPause();
        TripStore.get(getActivity()).updateSettings(mSettings);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        String settingsID = "";
        mSettings = TripStore.get(getActivity()).getSettings(settingsID);
        View v = inflater.inflate(R.layout.fragment_settings, container, false);

        mIDField = (EditText)v.findViewById(R.id.settings_id);
        mIDField.setText(mSettings.getId());
        mIDField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(
                    CharSequence s, int start, int count, int after) {
                // This space intentionally left blank
            }
            @Override
            public void onTextChanged(
                    CharSequence s, int start, int before, int count) {
                mSettings.setId(s.toString());
            }
            @Override
            public void afterTextChanged(Editable s) {
                // This one too
            }
        });

        mNameField = (EditText)v.findViewById(R.id.settings_name);
        mNameField.setText(mSettings.getName());
        mNameField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(
                    CharSequence s, int start, int count, int after) {
                // This space intentionally left blank
            }
            @Override
            public void onTextChanged(
                    CharSequence s, int start, int before, int count) {
                mSettings.setName(s.toString());
            }
            @Override
            public void afterTextChanged(Editable s) {
                // This one too
            }
        });

        mEmailField = (EditText)v.findViewById(R.id.settings_email);
        mEmailField.setText(mSettings.getEmail());
        mEmailField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(
                    CharSequence s, int start, int count, int after) {
                // This space intentionally left blank
            }
            @Override
            public void onTextChanged(
                    CharSequence s, int start, int before, int count) {
                mSettings.setEmail(s.toString());
            }
            @Override
            public void afterTextChanged(Editable s) {
                // This one too
            }
        });

        mCommentField = (EditText)v.findViewById(R.id.settings_comment);
        mCommentField.setText(mSettings.getComment());
        mCommentField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(
                    CharSequence s, int start, int count, int after) {
                // This space intentionally left blank
            }
            @Override
            public void onTextChanged(
                    CharSequence s, int start, int before, int count) {
                mSettings.setComment(s.toString());
            }
            @Override
            public void afterTextChanged(Editable s) {
                // This one too
            }
        });

        mGenderGroup = (RadioGroup)v.findViewById(R.id.genderGroup);
        switch(mSettings.getGender()) {
            case 0:
                mGenderGroup.check(R.id.genderMale);
                break;

            case 1:
                mGenderGroup.check(R.id.genderFemale);
                break;

            default:
                mGenderGroup.check(R.id.genderMale);
                break;
        }
        mGenderGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.genderMale){
                    mSettings.setGender(0);
                }else if (i == R.id.genderFemale){
                    mSettings.setGender(1);
                }
            }
        });

        return v;
    }
}
