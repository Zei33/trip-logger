package net.zeiworld.triplogger.database;

public class TripDbSchema {

    public static final class TripTable {
        public static final String NAME = "trips";
        public static final String SETTINGSNAME = "settings";

        public static final class Cols {
            public static final String UUID = "uuid";
            public static final String TITLE = "title";
            public static final String DATE = "date";
            public static final String TYPE = "type";
            public static final String DESTINATION = "destination";
            public static final String DURATION = "duration";
            public static final String COMMENT = "comment";
            public static final String PHOTO = "photo";
            public static final String COMPLETED = "completed";
            public static final String LAT = "latitude";
            public static final String LON = "longitude";
            public static final String DISPLAYNAME = "displayname";
            public static final String ID = "id";
            public static final String EMAIL = "email";
            public static final String GENDER = "gender";
        }
    }

}
