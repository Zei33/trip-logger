package net.zeiworld.triplogger.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import net.zeiworld.triplogger.Settings;
import net.zeiworld.triplogger.Trip;

import java.util.Date;
import java.util.UUID;

public class TripCursorWrapper extends CursorWrapper {

    public TripCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Trip getTrip() {
        String uuidString = getString(getColumnIndex(TripDbSchema.TripTable.Cols.UUID));
        String title = getString(getColumnIndex(TripDbSchema.TripTable.Cols.TITLE));
        long date = getLong(getColumnIndex(TripDbSchema.TripTable.Cols.DATE));
        int type = getInt(getColumnIndex(TripDbSchema.TripTable.Cols.TYPE));
        String destination = getString(getColumnIndex(TripDbSchema.TripTable.Cols.DESTINATION));
        int duration = getInt(getColumnIndex(TripDbSchema.TripTable.Cols.DURATION));
        String comment = getString(getColumnIndex(TripDbSchema.TripTable.Cols.COMMENT));
        String photo = getString(getColumnIndex(TripDbSchema.TripTable.Cols.PHOTO));
        int isCompleted = getInt(getColumnIndex(TripDbSchema.TripTable.Cols.COMPLETED));
        float lat = getFloat(getColumnIndex(TripDbSchema.TripTable.Cols.LAT));
        float lon = getFloat(getColumnIndex(TripDbSchema.TripTable.Cols.LON));


        Trip trip = new Trip(UUID.fromString(uuidString));
        trip.setTitle(title);
        trip.setDate(new Date(date));
        trip.setType(new Integer(type));
        trip.setDestination(destination);
        trip.setDuration(duration);
        trip.setComment(comment);
        trip.setPhoto(photo);
        trip.setComplete(isCompleted != 0);
        trip.setCoords(lat, lon);
        return trip;
    }

    public Settings getSettings(){
        String id = getString(getColumnIndex(TripDbSchema.TripTable.Cols.ID));
        String name = getString(getColumnIndex(TripDbSchema.TripTable.Cols.DISPLAYNAME));
        String email = getString(getColumnIndex(TripDbSchema.TripTable.Cols.EMAIL));
        String comment = getString(getColumnIndex(TripDbSchema.TripTable.Cols.COMMENT));
        Integer gender = getInt(getColumnIndex(TripDbSchema.TripTable.Cols.GENDER));

        Settings settings = new Settings(id);
        settings.setName(name);
        settings.setEmail(email);
        settings.setComment(comment);
        settings.setGender(gender);
        return settings;
    }

}
