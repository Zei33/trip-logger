package net.zeiworld.triplogger.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TripBaseHelper extends SQLiteOpenHelper {

    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "tripBase.db";
    public TripBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TripDbSchema.TripTable.NAME + "(" +
                " _id integer primary key autoincrement, " +
                TripDbSchema.TripTable.Cols.UUID + ", " +
                TripDbSchema.TripTable.Cols.TITLE + ", " +
                TripDbSchema.TripTable.Cols.DATE + ", " +
                TripDbSchema.TripTable.Cols.TYPE + ", " +
                TripDbSchema.TripTable.Cols.DESTINATION + ", " +
                TripDbSchema.TripTable.Cols.DURATION + ", " +
                TripDbSchema.TripTable.Cols.COMMENT + ", " +
                TripDbSchema.TripTable.Cols.PHOTO + ", " +
                TripDbSchema.TripTable.Cols.COMPLETED + ", " +
                TripDbSchema.TripTable.Cols.LAT + ", " +
                TripDbSchema.TripTable.Cols.LON +
                ")");
        db.execSQL("CREATE TABLE " + TripDbSchema.TripTable.SETTINGSNAME + "(" +
                TripDbSchema.TripTable.Cols.ID + ", " +
                TripDbSchema.TripTable.Cols.DISPLAYNAME + ", " +
                TripDbSchema.TripTable.Cols.EMAIL + ", " +
                TripDbSchema.TripTable.Cols.COMMENT + ", " +
                TripDbSchema.TripTable.Cols.GENDER +
                ")");
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

}
