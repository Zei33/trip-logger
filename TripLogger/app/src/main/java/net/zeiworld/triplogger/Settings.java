package net.zeiworld.triplogger;

import java.util.Date;
import java.util.UUID;

public class Settings {
    private String mId;
    private String mName;
    private String mComment;
    private String mEmail;
    private Integer mGender;

    public Settings() {
        this("1067958");
    }

    public Settings(String id) {
        mId = id;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String comment) {
        mComment = comment;
    }

    public Integer getGender() {
        if (mGender != null) { return mGender; }else{ return 0; }
    }

    public void setGender(Integer gender) {
        mGender = gender;
    }


}
