package net.zeiworld.triplogger;

import java.util.Date;
import java.util.UUID;

public class Trip {
    private UUID mId;
    private String mTitle;
    private Date mDate;
    private Integer mType;
    private String mDestination;
    private Integer mDuration;
    private String mComment;
    private String mPhoto;
    private boolean mComplete;
    private float mLat;
    private float mLon;

    public Trip() {
        this(UUID.randomUUID());
    }

    public Trip(UUID id) {
        mId = id;
        mDate = new Date();
    }

    public String getPhotoFilename() {
        return "IMG_" + getId().toString() + ".jpg";
    }

    public UUID getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public Integer getType() { return mType; }

    public void setTypeString(String type) { mType = Integer.parseInt(type); }

    public void setType(Integer type) { mType = type; }

    public String getDestination() {
        return mDestination;
    }

    public void setDestination(String destination) {
        mDestination = destination;
    }

    public Integer getDuration() { return mDuration; }

    public void setDuration(Integer duration) { mDuration = duration; }

    public String getComment() {
        return mComment;
    }

    public void setComment(String comment) {
        mComment = comment;
    }

    public String getPhoto() {
        return mPhoto;
    }

    public void setCoords(float lat, float lon) { mLat = lat; mLon = lon; }

    public float getLat() { return mLat; }

    public float getLon() { return mLon; }

    public void setPhoto(String photo) {
        mPhoto = photo;
    }

    public boolean isComplete() {
        return mComplete;
    }

    public void setComplete(boolean solved) {
        mComplete = solved;
    }
}
